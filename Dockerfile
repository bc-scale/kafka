FROM alpine:latest AS kafka
WORKDIR /opt
COPY scripts/* .
RUN ["sh","build"]
VOLUME /data
WORKDIR /opt/kafka/bin
ENTRYPOINT ["sh","run"]
